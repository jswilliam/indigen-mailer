var express = require("express");
var router = express.Router();
var nodemailer = require("nodemailer");
const creds = require("../config/config");
var hbs = require("nodemailer-express-handlebars");
var path = require("path");

var transport = {
  host: "smtp.mailgun.org",
  port: 587,
  secure: false,
  auth: {
    user: creds["USER"],
    pass: creds["PASS"]
  }
};

var transporter = nodemailer.createTransport(transport);

transporter.verify((error, success) => {
  if (error) {
    console.log(error);
  } else {
    console.log("Server is ready to take messages");
  }
});

router.post("/send", (req, res, next) => {
  var subject = req.body.subject;
  var to = req.body.to;
  var from = req.body.from;
  var message = req.body.message;

  var mail = {
    from: from,
    to: to,
    subject: subject,
    context: {
      text: message
    },
    template: "index"
  };

  const handlebarOptions = {
    viewEngine: {
      extName: ".hbs",
      partialsDir: "views/",
      layoutsDir: "views/",
      defaultLayout: "index.hbs"
    },
    viewPath: "views/",
    extName: ".hbs"
  };

  transporter.use("compile", hbs(handlebarOptions));

  transporter.sendMail(mail, (err, data) => {
    if (err) {
      res.json({
        msg: "fail"
      });
      console.log(err);
    } else {
      res.json({
        msg: "success"
      });
      console.log(data);
      console.log(mail);
    }
  });
});

module.exports = router;
