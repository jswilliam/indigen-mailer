import React, { Component } from "react";
import axios from "axios";
import {
  Select,
  PageHeader,
  Input,
  Form,
  Button,
  Alert,
  Spin,
  Icon
} from "antd";
const { TextArea } = Input;
const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

class MailForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subject: "",
      from: "",
      to: [],
      message: "",
      error: null,
      loading: false
    };
  }
  handleSubmit(e) {
    e.preventDefault();
    this.setState({ loading: true });
    axios({
      method: "POST",
      url: "http://localhost:3002/send",
      data: {
        subject: this.state.subject,
        from: this.state.from,
        to: this.state.to.join(","),
        message: this.state.message
      }
    }).then(response => {
      if (response.data.msg === "success") {
        this.setState({
          error: false,
          loading: false,
          subject: "",
          from: "",
          to: [],
          message: ""
        });
      } else if (response.data.msg === "fail") {
        this.setState({ error: true, loading: false });
      }
    });
  }

  render() {
    return (
      <div className="col-sm-4 offset-sm-4">
        <PageHeader title="Indigen" subTitle="Mail Form" />
        {this.state.error !== null && (
          <Alert
            message={
              this.state.error ? "Message failed to send." : "Message Sent."
            }
            type={this.state.error ? "error" : "success"}
            closable
            showIcon
          />
        )}
        <Form id="contact-form">
          <Form.Item>
            <Input
              type="text"
              id="subject"
              placeholder="Subject"
              value={this.state.subject}
              disabled={this.state.loading}
              onChange={e => this.setState({ subject: e.target.value })}
            />
          </Form.Item>
          <Form.Item>
            <Input
              type="email"
              id="from"
              placeholder="From"
              value={this.state.from}
              disabled={this.state.loading}
              onChange={e => this.setState({ from: e.target.value })}
            />
          </Form.Item>
          <Form.Item>
            <Select
              mode="tags"
              id="to"
              style={{ width: "100%" }}
              placeholder="To"
              value={this.state.to}
              disabled={this.state.loading}
              onChange={value => this.setState({ to: value })}
            />
          </Form.Item>
          <Form.Item>
            <TextArea
              autoSize={{ minRows: 3, maxRows: 10 }}
              id="message"
              placeholder="Message.."
              value={this.state.message}
              disabled={this.state.loading}
              onChange={e => this.setState({ message: e.target.value })}
            ></TextArea>
          </Form.Item>
          <Button
            onClick={this.handleSubmit.bind(this)}
            disabled={this.state.loading}
          >
            Send
            {this.state.loading && <Spin indicator={antIcon} />}
          </Button>
        </Form>
      </div>
    );
  }
}

export default MailForm;
